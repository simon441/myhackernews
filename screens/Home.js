/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import Buttons from '../components/Buttons';
import SearchForm from '../components/SearchForm';
import Stories from '../components/Stories';
import {useGlobalContext} from '../context/context';

const Home = () => {
  const {isLoading} = useGlobalContext();
  return (
    <>
      <SearchForm />
      <ActivityIndicator size="large" animating={true} />
      {isLoading ? (
        <>
          <View
            style={[
              {flex: 1, justifyContent: 'center'},
              {
                flexDirection: 'row',
                justifyContent: 'space-around',
                padding: 10,
                zIndex: 4,
              },
            ]}>
            <Text>Loading...</Text>
            <ActivityIndicator
              style={{zIndex: 5}}
              size="large"
              color="#0000ff"
            />
          </View>
        </>
      ) : (
        <>
          <Buttons />
          <Stories />
          <Buttons />
        </>
      )}
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
