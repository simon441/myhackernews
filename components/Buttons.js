import React from 'react';
import {Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useGlobalContext} from '../context/context';

const Buttons = () => {
  const {isLoading, page, nbPages, handlePage} = useGlobalContext();

  return (
    <View style={styles.btnContainer}>
      <Button
        disabled={isLoading || page === 0}
        onPress={() => handlePage('dec')}
        title="prev"
      />
      {/* <TouchableOpacity disabled={isLoading} onPress={() => handlePage('dec')}>
        <Text>prev</Text>
      </TouchableOpacity> */}
      <Text style={styles.currentPage}>
        {page + 1} of {nbPages}
      </Text>
      <Button
        disabled={isLoading || page === nbPages - 1}
        onPress={() => handlePage('inc')}
        title="next"
      />
      {/* <TouchableOpacity disabled={isLoading} onPress={() => handlePage('inc')}>
        <Text>next</Text>
      </TouchableOpacity> */}
    </View>
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    width: '90%',
    maxWidth: 1170,
    margin: 0,
    marginHorizontal: '5%',
    marginBottom: 15,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  currentPage: {
    marginBottom: 0,
    // fontSize: 16, // 1.2rem
    fontWeight: 'bold',
  },
  button: {
    margin: 20, // 1rem,
    //padding: 0.25rem 0.5rem;
    paddingVertical: 12,
    paddingHorizontal: 16,
    textTransform: 'capitalize',
    fontWeight: '700',
    borderColor: 'transparent',
    backgroundColor: '#49a6e9',
    borderRadius: 6, //0.25rem,
    color: '#fff',
    letterSpacing: 3, // 0.1rem
  },
});

export default Buttons;
