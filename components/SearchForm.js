import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {useGlobalContext} from '../context/context';
import {style} from '../Style';

const SearchForm = () => {
  const {query, handleSearch} = useGlobalContext();

  return (
    <View style={styles.searchForm}>
      <Text style={[style.h2, styles.title]}>search hacker news</Text>
      <TextInput
        autoCapitalize="words"
        style={styles.formInput}
        value={query}
        onChangeText={value => handleSearch(value)}
        onEndEditing={() => handleSearch(query)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  searchForm: {
    width: '90%',
    maxWidth: 1170,
    marginHorizontal: '5%',
    marginTop: 20, //5rem,
    marginBottom: 30, //3rem,
  },
  formInput: {
    width: '100%',
    // borderStyle: '',
    borderColor: '#bcccdc', //3px solid var(--clr - grey - 8),
    borderWidth: 3,
    borderRadius: 10,
    maxWidth: 600,
    backgroundColor: 'transparent',
    padding: 6, //.1rem,
    fontSize: 16, //1rem,
    color: '#324d67', // var(--clr - grey - 3),
    // textTransform: 'uppercase',
    // letterSpacing: 6, //var(--spacing),
    marginTop: 16, //1rem,
  },
});

export default SearchForm;
