import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  Linking,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {style, customVars} from '../Style';

import {useGlobalContext} from '../context/context';

const Stories = () => {
  const {isLoading, hits, removeStory} = useGlobalContext();

  const openUrl = async url => {
    const open = () => {
      if (Linking.canOpenURL(url)) {
        Linking.openURL(url).then();
      } else {
        Alert.alert('Hacker news search', `Cannot open url: ${url}`);
      }
    };
    Alert.alert(
      'Hacker new search',
      `Open ${url}?`,
      [
        {
          text: 'NO',
          onPress: () => {},
          style: 'cancel',
        },
        {text: 'YES', onPress: () => open()},
      ],
      {cancelable: false},
    );
  };

  const handleRemoveStory = (objectID, title) => {
    Alert.alert(
      'Hacker new search',
      `Remove story ${title}`,
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => removeStory(objectID)},
      ],
      {cancelable: true},
    );
  };

  if (isLoading) {
    return <ActivityIndicator />;
  }
  return (
    <>
      <ScrollView>
        {hits.map(story => {
          const {objectID, title, num_comments, url, points, author} = story;
          return (
            <View key={objectID} style={styles.story}>
              <Text style={[style.h4, styles.title]}>{title}</Text>
              <View style={styles.info}>
                <Text>
                  {points} points by <Text>{author} | </Text> {num_comments}{' '}
                  comments
                </Text>
              </View>
              {/* <View>
              <TouchableOpacity style={styles.readLink}>
                <Text onPress={() => openUrl(url)} style={styles.readLink}>
                  read more
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.removeBtn}
                onPress={() => removeStory(objectID)}>
                <Text style={styles.removeBtn}>Remove</Text>
              </TouchableOpacity>
            </View> */}
              <View style={styles.storyButtonContainer}>
                <View style={styles.storyButtonAction}>
                  <Button onPress={() => openUrl(url)} title="Read More" />
                </View>
                <View style={styles.storyButtonAction}>
                  <Button
                    color="red"
                    onPress={() => handleRemoveStory(objectID)}
                    title="remove"
                  />
                </View>
              </View>
            </View>
          );
        })}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    color: '#111',
    // marginTop: 15,
    fontSize: 20,
    // lineHeight: 18, //1.5,
    marginBottom: 14, //0.25rem,
  },
  stories: {
    width: '90%',
    // maxWidth: 1170,
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    marginHorizontal: '5%',
    marginBottom: 20,

    //5rem,
    // display: grid,
    // gap: 2rem,
  },
  // @media screen and (min-width: 992px) {
  //   .stories {
  //     display: grid,
  //     grid-template-columns: 1fr 1fr,
  //     /* align-items: start, */
  //   }
  // }
  story: {
    backgroundColor: customVars['--clr-white'],
    borderRadius: customVars['--radius'],
    // padding: //1rem 2rem,
    paddingVertical: 14,
    paddingHorizontal: 20,

    paddingTop: 15,
    borderBottomColor: 'blue',
    // borderBottomWidth: 10,
    marginBottom: 20,
  },
  storyButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  storyButtonAction: {
    width: '49%',
  },

  info: {
    marginBottom: 16, //0.5rem,
    color: customVars['--clr-grey-5'],
  },

  readLink: {
    fontSize: 16, //0.85rem,
    marginRight: 18, //0.75rem,
    textTransform: 'capitalize',
    color: customVars['--clr-primary-5'],
  },

  removeBtn: {
    backgroundColor: 'transparent',
    color: customVars['--clr-red-dark'],
    borderColor: 'transparent',
    textTransform: 'capitalize',
    fontSize: 16, //0.85rem,
  },
});

export default Stories;
