import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  h1: {},
  h2: {},
  h3: {},
  h4: {fontSize: 18},
});

export const customVars = {
  '--spacing': 6, //0.1rem;
  '--radius': 10, //0.25rem;
  '--clr-white': '#fff',
  '--clr-grey-5': 'hsl(210, 22%, 49%)',
  '--clr-primary-5': 'hsl(205, 78%, 60%)',
  '--clr-red-dark': 'hsl(360, 67%, 44%)',
};
